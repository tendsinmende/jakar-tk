# Jakar-Tk

A widget toolkit based on Rust and Vulkano. The goal is to give developers a fast and low-ish level window toolkit to build GUIs for Rust and Rust+Vulkano applications.

## Design

Every widget has to implement the `Widget` trait. The interface is a tree of widgets which gets rendered top-down. Before a widget is added to a parent widget, its behavior can be set (For instance, onCLick functions for a button).
The UI is rendered with Vsync at (if possible) 60fps.

## Road to 0.1 release

- [ ] Add `Widget` trait
- [ ] Implement test widgets
- [ ] Implement a renderer
- [ ] Implement text rendering
- [ ] Add input handling (as specially directing an input to the correct widget)
- [ ] Implement theming. (Maybe a combination of a Color-table and a texture-atlas)
- [ ] Write a guide for custom Widget implementations (maybe a "Diagram-Render" widget)


## Projects that currently use it

As far as I know the project is only going to be used in my [hobby engine](https://gitlab.com/Siebencorgie/jakar-engine) for the editor.

## Build and testing
You can build the project like every other Cargo project by executing:
```
cargo build``` or ```cargo build --release
```


However, most newcomers want to see something, for that execute the example with
```
cargo run --example ui_test --release
```

## License

You can decide if you want to use MIT or Apache License, Version 2.0.

However, it would be nice to credit the project if you use it on something bigger :)