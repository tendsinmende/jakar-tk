use vulkano::command_buffer::{AutoCommandBufferBuilder, DynamicState};
use vulkano::device::{Device, Queue};
use vulkano::framebuffer::*;
use vulkano::image::{SwapchainImage};
use vulkano::instance::Instance;
use vulkano::swapchain::{
    AcquireError, PresentMode, Surface, SurfaceTransform, Swapchain, SwapchainAcquireFuture,
    SwapchainCreationError,
};
use vulkano::sync::GpuFuture;

use vulkano;

use vulkano_win;
use vulkano_win::VkSurfaceBuild;
use winit::{EventsLoop, Window, WindowBuilder};

use std::mem;
use std::sync::Arc;

use widget::Widget;

/// Hosts the rendering system.
/// Is currently a bare bone vulkan(o) renderer. Only renders on one queue a build command buffer.
///
/// Currently there is one window per renderer, later you'll be able to specify several windows and the draw command will need information which window to draw to.
pub struct RenderHost {
    window_surface: Arc<Surface<Window>>,
    device: Arc<Device>,
    queue: Arc<Queue>,
    swapchain: Arc<Swapchain<Window>>,
    images: Vec<Arc<SwapchainImage<Window>>>,
    framebuffer: Vec<Arc<FramebufferAbstract + Send + Sync>>,
    render_pass: Arc<RenderPassAbstract + Send + Sync>,
    events_loop: EventsLoop,
    dynamic_state: DynamicState,
    ///is true if the swapchain needs to be recreated
    recreate_swapchain: bool,
}

impl RenderHost {
    ///Creates a brand new renderer to be used with a widget tree and opens an assosiated window.
    pub fn new() -> Self {
        // Basic initialization. TODO make a bit more robust?
        let instance = {
            let extensions = vulkano_win::required_extensions();
            Instance::new(None, &extensions, None).expect("failed to create Vulkan instance")
        };
        let physical = vulkano::instance::PhysicalDevice::enumerate(&instance)
            .next()
            .expect("no device available");

        let events_loop = EventsLoop::new();
        let window = WindowBuilder::new()
            .build_vk_surface(&events_loop, instance.clone())
            .unwrap();

        let dimensions = {
            let window = window.window();
            let factor = window.get_hidpi_factor();
            let (width, height) = window.get_inner_size().unwrap().to_physical(factor).into();
            [width, height]
        };

        let queue_family = physical
            .queue_families()
            .find(|&q| q.supports_graphics() && window.is_supported(q).unwrap_or(false))
            .expect("couldn't find a graphical queue family");

        let (device, mut queues) = {
            let device_ext = vulkano::device::DeviceExtensions {
                khr_swapchain: true,
                ..vulkano::device::DeviceExtensions::none()
            };

            Device::new(
                physical,
                physical.supported_features(),
                &device_ext,
                [(queue_family, 0.5)].iter().cloned(),
            )
            .expect("failed to create device")
        };

        let queue = queues.next().unwrap();

        let (swapchain, images) = {
            let caps = window
                .capabilities(physical)
                .expect("failed to get surface capabilities");
            let alpha = caps.supported_composite_alpha.iter().next().unwrap();
            let format = caps.supported_formats[0].0;
            Swapchain::new(
                device.clone(),
                window.clone(),
                caps.min_image_count,
                format,
                dimensions,
                1,
                caps.supported_usage_flags,
                &queue,
                SurfaceTransform::Identity,
                alpha,
                PresentMode::Fifo,
                true,
                None,
            )
            .expect("failed to create swapchain")
        };
        let render_pass = Arc::new(
            vulkano::single_pass_renderpass!(
             device.clone(),
             attachments: {
                 color: {
                    load: Clear,
                     store: Store,
                     format: swapchain.format(),
                     samples: 1,
                 }
             },
             pass: {
                 color: [color],
                 depth_stencil: {}
             }
         )
            .unwrap(),
        );

        let mut dynamic_state = DynamicState {
            line_width: None,
            viewports: None,
            scissors: None,
        };
        let mut framebuffer: Vec<Arc<FramebufferAbstract + Send + Sync>> = Vec::new();
        for img in images.iter() {
            let new = Arc::new(
                Framebuffer::start(render_pass.clone())
                    .add(img.clone())
                    .expect("failed to add image to FB image")
                    .build()
                    .expect("Failed to build framebuffer image"),
            );
            framebuffer.push(new);
        }

        RenderHost {
            window_surface: window,
            device: device,
            queue: queue,
            swapchain: swapchain,
            images: images,
            framebuffer: framebuffer,
            dynamic_state: dynamic_state,
            render_pass: render_pass,
            events_loop: events_loop,
            recreate_swapchain: false,
        }
    }

    ///Creates the render by taking already created device instance and window
    pub fn from_info() {
        unimplemented!();
    }

    ///Draws a widget tree to the RenderHosts's window.
    pub fn draw(&mut self) {
        //, widgets: Arc<Widget>){
        let mut cb = AutoCommandBufferBuilder::primary_one_time_submit(
            self.device.clone(),
            self.queue.family(),
        )
        .expect("Failed to create new cb");

        let (img_idx, future) = match self.check_swapchain() {
            Ok(res) => res,
            Err(e) => {
                println!("{:?}", e);
                self.recreate_swapchain = true;
                return;
            }
        };
        //Add clearing for the swapchain image
        cb = cb
            .begin_render_pass(
                self.framebuffer[img_idx].clone(),
                false,
                vec![[1.0, 0.0, 1.0, 1.0].into()],
            )
            .expect("failed to start renderpass");

        //TODO actually draw all the widgets.

        cb = cb.end_render_pass().expect("failed to end renderpass");

        let final_cb = cb.build().expect("Failed to build cb");

        let wait = future
            .then_execute(self.queue.clone(), final_cb)
            .expect("faile to execute")
            .then_signal_fence_and_flush()
            .expect("failed to signalfence")
            .then_swapchain_present(self.queue.clone(), self.swapchain.clone(), img_idx);

        let _ = wait.then_signal_fence().wait(None);
    }

    ///Recreates swapchain for the window size.
    ///Returns true if successfully recreated chain
    fn recreate_swapchain(&mut self) -> bool {
        //get new dimmensions etc

        //Update the widow dimensions in scope to prevent locking
        let new_dimensions = self
            .window_surface
            .window()
            .get_inner_size()
            .expect("failed to get window inner size for swapchain recreation");

        let (new_swapchain, new_images) =
        //cast dims to u32 since we need pixel, might lose some info
        match self.swapchain.recreate_with_dimension([new_dimensions.width as u32, new_dimensions.height as u32]) {
            Ok(r) => r,
            // This error tends to happen when the user is manually resizing the window.
            // Simply restarting the loop is the easiest way to fix this issue.
            Err(SwapchainCreationError::UnsupportedDimensions) => {
                return false;
            },
            Err(err) => panic!("{:?}", err)
        };

        //Now repace
        mem::replace(&mut self.swapchain, new_swapchain);
        mem::replace(&mut self.images, new_images);

        //Now recreate the Framebuffer based on the new swapchain images
        self.rebuild_framebuffer();

        //Now when can mark the swapchain as "fine" again
        self.recreate_swapchain = false;
        true
    }

    ///Recreates the current framebuffer based on the current swapchain images
    fn rebuild_framebuffer(&mut self) {
        let fb = self
            .images
            .iter()
            .map(|img| {
                Arc::new(
                    Framebuffer::start(self.render_pass.clone())
                        .add(img.clone())
                        .expect("failed to add image to fb")
                        .build()
                        .expect("failed to submit new fb image"),
                ) as Arc<FramebufferAbstract + Send + Sync>
            })
            .collect();

        self.framebuffer = fb;
    }

    ///Returns the image index and the acquire future if the image is up to date
    ///, if the image state is outdated it will return an error.
    ///Panics if another error occures while pulling a new image
    fn check_image_state(&self) -> Result<(usize, SwapchainAcquireFuture<Window>), AcquireError> {
        match vulkano::swapchain::acquire_next_image(self.swapchain.clone(), None) {
            Ok(r) => {
                return Ok(r);
            }
            Err(vulkano::swapchain::AcquireError::OutOfDate) => {
                return Err(vulkano::swapchain::AcquireError::OutOfDate);
            }
            Err(err) => panic!("{:?}", err),
        };
    }

    ///checks the pipeline. If not up to date (return is AcquireError), flags the render_host to recreate.
    /// the image at the next frame.
    fn check_swapchain(&mut self) -> Result<(usize, SwapchainAcquireFuture<Window>), AcquireError> {
        //If found out in last frame that images are out of sync, generate new ones
        if self.recreate_swapchain {
            if !self.recreate_swapchain() {
                //If we got the UnsupportedDimensions Error (and therefor returned false)
                //Abord the frame
                println!("failed to recreate new swapchain",);
                return Err(AcquireError::SurfaceLost);
            }
        }
        //Try to get a new image
        //If not possible becuase outdated (result is Err)
        //then return (abort frame)
        //and recreate swapchain
        match self.check_image_state() {
            Ok(r) => return Ok(r),
            Err(er) => {
                self.recreate_swapchain = true;
                return Err(er);
            }
        };
    }
}
