extern crate jakar_tk;
use jakar_tk::*;

use std::time::Duration;
use std::thread::sleep;
fn main(){
    println!("Hello ui test");
    let mut render = render_host::RenderHost::new();
    render.draw();
    sleep(Duration::from_secs(2));
    println!("Finished");
}
